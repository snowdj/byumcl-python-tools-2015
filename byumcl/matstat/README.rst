=======
matstat
=======

After becoming frustrated with scipy.stats using non-standard notation and functional forms for distributions, I wrote this small library with common distributions. Feel free to extend/add to this as you see fit and/or need.

All distributions currently in the library have been tested against MatLab and Mathematica to ensure correctness.
