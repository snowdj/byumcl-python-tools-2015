import numpy as np
import matplotlib.pyplot as plt
def cheb(n):
    '''
    This function creates all of the chebyshev polynomials up to order n.
    Chebyshev polynomials are defined recursively as:
    T_i+1(x) = 2 * x * T_i(x) - T_i-1(x)

    Parameters:
    -----------
    n: Order of polynomial that you want you to evaluate up to.

    Returns:
    --------
    chebfuncs: The Chebyshev polynomials up to order n.
               Python functions
    '''
    # Create a list where we will store our functions.
    chebfuncs = []

    # We must define T_0 and T_1 before we define the rest of the functions
    def t_0(x):
        if x is int:
            x = float(x)

        if x is float:
            return 1
        else:
            return np.zeros_like(x) + 1


    def t_1(x):
        if x is int:
            x = float(x)
        if x is float:
            return x
        else:
            return np.zeros_like(x) + x

    if n==0:
        chebfuncs.append(t_0)
        return tuple(chebfuncs)

    if n==1:
        chebfuncs.append(t_0)
        chebfuncs.append(t_1)
        return tuple(chebfuncs)

    else:

        # Append these to our chebfuncs list
        chebfuncs.append(t_0)
        chebfuncs.append(t_1)

        # We must recursively define the rest of the values.
        # We want our functions to be able to take in vectors of x values so
        # we need to use np.vectorize()

        for i in xrange(2, n):
            index = i

            def tempfunc(x, index=index):
                index_2 = index - 2
                index_1 = index - 1
                tempval = 2.0 * x * chebfuncs[index_1](x) - chebfuncs[index_2](x)

                return tempval

            tempfunc = np.vectorize(tempfunc)
            chebfuncs.append(tempfunc)
            del tempfunc

    return tuple(chebfuncs)
funcs = cheb(5)
