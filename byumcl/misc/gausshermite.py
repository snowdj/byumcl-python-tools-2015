"""
Created August 3, 2012

Author: Spencer Lyon

Gauss hermite quadrature
"""
from __future__ import division
import numpy as np
import numpy.linalg as la
from numpy.polynomial.hermite import hermcompanion, hermval, hermder

__all__ = ['gauss_hermite']


def hermgauss(deg):
    """
    NOTE: This function is taken from numpy!

    Gauss-Hermite quadrature.

    Computes the sample points and weights for Gauss-Hermite quadrature.
    These sample points and weights will correctly integrate polynomials of
    degree :math:`2*deg - 1` or less over the interval :math:`[-\inf, \inf]`
    with the weight function :math:`f(x) = \exp(-x^2)`.

    Parameters
    ----------
    deg : int
        Number of sample points and weights. It must be >= 1.

    Returns
    -------
    x : ndarray
        1-D ndarray containing the sample points.
    y : ndarray
        1-D ndarray containing the weights.

    Notes
    -----

    .. versionadded::1.7.0

    The results have only been tested up to degree 100, higher degrees may
    be problematic. The weights are determined by using the fact that

    .. math:: w_k = c / (H'_n(x_k) * H_{n-1}(x_k))

    where :math:`c` is a constant independent of :math:`k` and :math:`x_k`
    is the k'th root of :math:`H_n`, and then scaling the results to get
    the right value when integrating 1.

    """
    ideg = int(deg)
    if ideg != deg or ideg < 1:
        raise ValueError("deg must be a non-negative integer")

    # first approximation of roots. We use the fact that the companion
    # matrix is symmetric in this case in order to obtain better zeros.
    c = np.array([0] * deg + [1])
    m = hermcompanion(c)
    x = la.eigvals(m)
    x.sort()

    # improve roots by one application of Newton
    dy = hermval(x, c)
    df = hermval(x, hermder(c))
    x -= dy / df

    # compute the weights. We scale the factor to avoid possible numerical
    # overflow.
    fm = hermval(x, c[1:])
    fm /= np.abs(fm).max()
    df /= np.abs(df).max()
    w = 1 / (fm * df)

    # for Hermite we can also symmetrize
    w = (w + w[::-1]) / 2
    x = (x - x[::-1]) / 2

    # scale w to get the right value
    w *= np.sqrt(np.pi) / w.sum()

    return x, w


def gauss_hermite(N, mu=0, sigma=1):
    """
    Compute the nodes and weights for gauss-hermite quadrature.
    It is assumed that the variable you are trying to model is
    distributed normally with mean = mu and variance = sigma.

    Parameters
    ----------
    N: int
        The number of nodes in the support and weight vectors.

    mu: float, optional(default=0)
        The mean of the random variable.

    sigma: float, optional(default=1)
         The standard deviation of the random variable.

    Returns
    -------
    eps: array_like, dtype=float, shape=(N,)
        The support vector for the random variable.

    weights: array_like, dtype=float, shape=(N,)
        The weights associated with the nodes in eps.

    Notes
    -----
    This function calls numpy.polynomial.hermite.hermgauss to create an
    initial version of the eps, and weights vectors. The numpy function
    assumes the random variable follows the standard normal
    distribution.

    We take the results of that function and apply the following
    transformation to the nodes (assume eps_numpy are the return values
    from the numpy func):
        eps = mu + sigma * eps_numpy

    The weights are unchanged in the transformation
    """
    eps_numpy, weights = hermgauss(N)

    eps = mu + sigma * eps_numpy

    # Make sure probabilities sum to 1
    weights /= sum(weights)

    return [eps, weights]
